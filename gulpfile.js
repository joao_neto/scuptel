var gulp            = require('gulp');
var del             = require('del');
var jshint          = require('gulp-jshint');
var jscs            = require('gulp-jscs');
var stylish         = require('gulp-jscs-stylish');
var inject          = require('gulp-inject');
var uglify          = require('gulp-uglify');
var concat          = require('gulp-concat');
var less            = require('gulp-less');
var minifyCSS       = require('gulp-minify-css');
var angularFilesort = require('gulp-angular-filesort');
var templateCache   = require('gulp-angular-templatecache');
var runSequence     = require('run-sequence');
var browserSync     = require('browser-sync');
// var pkg             = require('./package.json');
var assets          = require('./config/assets');
var KarmaServer     = require('karma').Server;
var bower           = require('bower');

// Clean all
gulp.task('clean', function(done) {
  return del([
    assets.paths.assets,
    assets.paths.bowerComponents,
    './dist'
  ], done);
});

// Install bower_components
gulp.task('bower', function() {
  return bower.commands.install();
});

// Copy assets files to app
gulp.task('assets', function() {
  return gulp.src(assets.files.assets, {base: 'bower_components'})
    .pipe(gulp.dest(assets.paths.assets));
});

// Copy assets files to dist
gulp.task('copy', function() {
  return gulp.src('./app/assets/**/*')
    .pipe(gulp.dest('./dist/assets'));
});

// Compress js
gulp.task('compress', function() {
  return gulp.src([
      './app/scripts/**/*.js',
      '!./app/scripts/*.spec.js',
      '!./app/scripts/templates.js'
    ])
    .pipe(angularFilesort())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/scripts'));
});

// Dist app
gulp.task('dist', function(done) {
  runSequence(
    'clean',
    'bower',
    'assets',
    'copy',
    'cache',
    'compress',
    'dist:index',
    done);
});

// Dist app for development
gulp.task('dist:app', function(done) {
  runSequence(
    'bower',
    'assets',
    'compress',
    'app:index',
    done);
});

// Generate index to dist
gulp.task('dist:index', function() {
  return gulp.src('./config/index.html')
    .pipe(inject(
      gulp.src('./dist/assets/**/*', {read: false}), {
        name: 'assets',
        ignorePath: 'dist',
        addRootSlash: false
      })
    )
    .pipe(inject(
      // Inject dist css
      gulp.src('./dist/css/*.css', {read: false}), {
        name: 'app_css',
        ignorePath: 'dist',
        addRootSlash: false
      })
    )
    .pipe(inject(
      // Inject dist scripts
      gulp.src('./dist/scripts/*.js').pipe(angularFilesort()), {
        name: 'app_js',
        ignorePath: 'dist',
        addRootSlash: false
      })
    ).pipe(gulp.dest('./dist'));
});

// Generate index to app
gulp.task('app:index', function() {
  return gulp.src('./config/index.html')
    .pipe(inject(
      gulp.src(assets.files.assets, {read: false}), {
        name: 'assets',
        ignorePath: 'bower_components',
        addPrefix: 'assets',
        addRootSlash: false
      })
    )
    .pipe(inject(
      // Inject app css
      gulp.src(assets.files.app.css, {read: false}), {
        name: 'app_css',
        ignorePath: 'app',
        addRootSlash: false
      })
    )
    .pipe(inject(
      // Inject app scripts
      gulp.src(assets.files.app.js).pipe(angularFilesort()), {
        name: 'app_js',
        ignorePath: 'app',
        addRootSlash: false
      })
    ).pipe(gulp.dest('./app'));
});

// Template caches
gulp.task('cache', function() {
  return gulp.src('./app/**/*.tpl.html')
    .pipe(templateCache({standalone: true}))
    .pipe(gulp.dest('./dist/scripts'));
});

// Code lint
gulp.task('jshint', function() {
  return gulp.src([
    './**/*.js',
    '!./node_modules/**/*',
    '!./bower_components/**/*',
    '!./app/assets/**/*',
    '!./dist/**/*'
  ])
    .pipe(jshint('.jshintrc'))
    .pipe(jscs('.jscsrc'))
    .on('error', function() {})
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter('jshint-stylish'));
});

// Compile app css
gulp.task('less', function() {
  return gulp.src('./app/scripts/app.less')
    .pipe(less())
    .pipe(minifyCSS())
    // .pipe(less({
    //   paths: ['./app/less/includes']
    // }))
    .pipe(gulp.dest('./app/css'));
});

// Automated tests
gulp.task('test', ['dist:app'], function(done) {
  new KarmaServer({
    configFile: __dirname + '/config/karma.conf.js',
    singleRun: true
  }, done).start();
});

// Serve source app for development
gulp.task('serve', ['jshint', 'less', 'dist:app'], function() {
  browserSync({server: './app'});
  gulp.watch('./app/**/*.less', ['less']);
  gulp.watch('./app').on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
