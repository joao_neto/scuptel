ScupTel [![wercker status](https://app.wercker.com/status/52a8ec32f1f7a745289d1be1470ec862/s/master "wercker status")](https://app.wercker.com/project/bykey/52a8ec32f1f7a745289d1be1470ec862)
=======

Frontend stack example

To start app in development mode:

```
npm install && npm start
```


To start app in dist:

```
npm install && npm install http-server -g && ./node_modules/.bin/gulp dist && hs ./dist
```

To run tests:

```
npm install && npm test
```
