(function() {
  'use strict';

  angular.module('home', ['ui.router', 'services.combinations', 'filters'])
    .controller('ContentController', ContentController)
    .config(homeConfig);

  ContentController.$inject = ['$scope', 'combinations', 'details', 'plans'];
  function ContentController($scope, combinations, details, plans) {
    var vm = this;

    vm.details = details;
    vm.plans = plans;
    vm.combinations = combinations;

    $scope.$watch('vm.form', function(val) {
      if (val) {
        var originDdd = val.origin && val.origin.ddd;
        var destinyDdd = val.destiny && val.destiny.ddd;
        combinations.updatePricing(originDdd, destinyDdd);
      }
      console.log(val);
    }, true);
  }

  homeConfig.$inject = ['$stateProvider'];
  function homeConfig($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: '/',
        views: {
          'content': {
            templateUrl: 'scripts/home/content.tpl.html',
            controller: 'ContentController',
            controllerAs: 'vm',
            resolve: {
              details: ['combinations', function(combinations) {
                return combinations.getDetails();
              }],
              pricing: ['combinations', function(combinations) {
                return combinations
                  .getPricingTable()
                  .then(combinations.setPricingCache);
              }],
              plans: ['combinations', function(combinations) {
                return combinations.getPlans();
              }]
            }
          }
        }
      });
  }

})();
