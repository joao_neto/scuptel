describe('filters', function() {
  'use strict';

  var $filter;

  beforeEach(function() {
    module('filters');

    inject(function(_$filter_) {
      $filter = _$filter_;
    });
  });

  describe('moneyFormatBR', function() {
    it('should convert to BRL format', function() {
      var result = $filter('moneyFormatBR')(10.10).$$unwrapTrustedValue();
      expect(result).toEqual('R$ <span class="price">10</span>,10');
    });

    it('should convert thousands to BRL format', function() {
      var result = $filter('moneyFormatBR')(1000.10).$$unwrapTrustedValue();
      expect(result).toEqual('R$ <span class="price">1.000</span>,10');
    });

    it('should convert `string float` number to BRL format', function() {
      var result = $filter('moneyFormatBR')('1000000.1').$$unwrapTrustedValue();
      expect(result).toEqual('R$ <span class="price">1.000.000</span>,10');
    });

    it('should convert invlid string float number to BRL format', function() {
      function invalidFloat() {
        $filter('moneyFormatBR')('INVALID_FLOAT').$$unwrapTrustedValue();
      }
      expect(invalidFloat).toThrow();
    });
  });
});
