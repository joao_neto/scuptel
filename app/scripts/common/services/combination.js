(function() {
  'use strict';

  angular.module('services.combinations', [])
    .factory('combinations', CombinationService);

  CombinationService.$inject = ['$http', '$rootScope', 'apiUrl'];
  function CombinationService($http, $rootScope, apiUrl) {
    var self = this;

    var service = {
      setPricingCache: function(pricing) {
        self._pricingCache = pricing.data;
        return pricing.data;
      },

      getPricing: function() {
        return self.pricing || [];
      },

      getPricingPlan: function(time, planTime) {
        if (!time || !self.pricing || !self.pricing.length) {
          return;
        }

        if (!planTime) {
          return self.pricing[0].price * time;
        }

        time -= planTime;
        time = time < 0 ? 0 : time;

        return self.pricing[0].price * time + (time * 0.1);
      },

      getPricingTable: function() {
        return $http.get(apiUrl + '/ddd/pricing').then(function(res) {
          return res.data;
        });
      },

      getPlans: function() {
        return $http.get(apiUrl + '/plans').then(function(res) {
          return res.data;
        });
      },

      getDetails: function() {
        return $http.get(apiUrl + '/ddd/details').then(function(res) {
          return res.data;
        });
      },

      updatePricing: function(origin, destiny) {
        if (destiny && origin) {
          var pricing = self._pricingCache.filter(function(price) {
            return (price.origin === origin && price.destiny === destiny);
          });

          self.pricing = pricing;
          return pricing[0];
        }
      }
    };

    return service;
  }

})();
