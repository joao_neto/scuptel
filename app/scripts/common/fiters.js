(function() {
  'use strict';
  angular.module('filters', [])
    .filter('moneyFormatBR', moneyFormatBRFilter);

  moneyFormatBRFilter.$inject = ['$sce'];
  function moneyFormatBRFilter($sce) {
    return function(val) {
      var fixedVal = parseFloat(val).toFixed(2);
      if (isNaN(fixedVal)) {
        throw new Error('invalid float value');
      }
      return $sce.trustAsHtml(
        'R$ ' + fixedVal.replace('.', ',')
        .replace(/(\d)(?=(\d{3})+\,)/g, '$1.')
        .replace(/^(.*)\,(.*)$/, '<span class="price">$1</span>,$2')
      );
    };
  }
})();
