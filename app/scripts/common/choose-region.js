(function() {
  'use strict';
  angular.module('choose-region', [])
    .directive('chooseRegion', DateSelectorDirective)
    .controller('ChooseRegionController', ChooseRegionController);

  function ChooseRegionController() {
    var modal = this;
    modal.modalHidden = true;

    modal.show = function() {
      modal.modalHidden = false;
    };

    modal.hide = function() {
      modal.modalHidden = true;
    };

    modal.choose = function(item) {
      modal.ngModel.$setViewValue(item);
      modal.ngModel.$render();
      modal.selected = item.ddd;
      modal.hide();
    };

    modal.setupNgModel = function(ngModel) {
      modal.ngModel = ngModel;
    };
  }

  function DateSelectorDirective() {
    return {
      restrict: 'ECA',
      replace: true,
      transclude: false,
      controller: ChooseRegionController,
      controllerAs: 'modal',
      require: ['?^ngModel', '^chooseRegion'],
      scope: {
        details: '='
      },
      link: function(scope, element, attrs, controllers) {
        var ngModel = controllers[0];
        var modal = controllers[1];
        modal.setupNgModel(ngModel);
      },
      templateUrl: 'scripts/common/choose-region.tpl.html'
    };
  }

})();
