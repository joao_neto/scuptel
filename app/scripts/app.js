(function() {
  'use strict';

  angular.module('app', [
    'ui.router',
    'home',
    'choose-region',
    'templates'
  ])
    .constant('apiUrl', 'http://private-fe2a-scuptel.apiary-mock.com')
    .controller('AppController', AppController)
    .config(appConfig);

  function AppController() {

  }

  appConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  function appConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
      .when('', '/')
      .otherwise('/');

    $stateProvider
      .state('app', {
        abstract: true,
        views: {
          '@': {
            templateUrl: 'scripts/app.tpl.html',
            controller: 'AppController',
            controllerAs: 'vm'
          },
          'header@app': {
            templateUrl: 'scripts/home/header.tpl.html',
          }
        }
      });
  }

})();
