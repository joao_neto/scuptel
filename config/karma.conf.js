module.exports = function(config) {
  config.set({
    basePath: '..',
    browsers: ['PhantomJS', /*'Chrome'*/],
    frameworks: ['jasmine'],
    files: [
      'bower_components/angular/angular.min.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-sanitize/angular-sanitize.min.js',
      'bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'bower_components/angular-messages/angular-messages.min.js',
      'app/scripts/**/*.js',
      'app/scripts/**/*.spec.js'
    ],
    colors: true,
    reporters: ['progress', 'dots'],
    singleRun: true
  });
};
